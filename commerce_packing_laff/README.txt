Introduction
---------------------

This is a module for better packing algorithm for the Drupal Commerce shipping 
modules. Commerce shipping modules only support the biggest box for packing. 
This is a huge problem when there are multiple products,with different sizes
on one order because is going to be more expensive for your customer. This
module have an algorithm that tries to fit the boxes (products) in a container 
(package) and then returns a height.

In this particular sub-module use the library "Largest Area Fit First (LAFF)"
This library is 'dumb' in that it just keeps chucking boxes at containers until
everything fits, then it returns the package needed (brute force method). 
I have been tested on order with 50+ product it would give you a time out. At 
the moment we only are supporting Commerce UPS and Commerce USPS.

Here is some information about the library.

Largest Area Fit First (LAFF) library:

This PHP class helps to solve the so called "3D packing problem"
often seen when packing containers with boxes (eg. for a webshop).
It calculates the approximate minimum container size needed to fit
the boxes and also gives additional information like on which level
they are packed.

The class was written using the "An Efficient Algorithm for 3D
Rectangular Box Packing" paper by M. Zahid Gürbüz, Selim Akyokus,
Ibrahim Emiroglu and Aysun Güran. It contains a step by step
explanation of the problem and the solution.

For more information about 3D packing
http://tinyurl.com/ltymhc8

Requirements
---------------------
This module requires the following modules:
 - Libraries (https://www.drupal.org/project/libraries)
 - Commerce Physical (https://www.drupal.org/project/commerce_physical)
 - Commerce (https://www.drupal.org/project/commerce)


Similar modules
---------------------
 - Packing (https://www.drupal.org/project/packaging) only for developers.


Installation
---------------------
 Install as you would normally install a contributed Drupal module. See:
 https://drupal.org/documentation/install/modules-themes/modules-7
 for further information.

 You may want to disable Toolbar module, since its output clashes with
 Administration Menu.

Configuration
---------------------
 

Troubleshooting
---------------------

FAQ
---------------------

 Maintainers
---------------------
Current maintainers:
* Darryl Norris (darol100) - https://www.drupal.org/u/darol100

